import { createStore } from 'redux';
import {
    File,
    Folder,
    Permission,
    VirtFS
} from '../../lib/virtfs/virtfs.js';
import {ROOTFS} from './rootfs.js';

let vfs = VirtFS({
    isroot: false,
    filesystem: ROOTFS
});

const initialState = {
    vfs: vfs
};

function reducer(state = initialState, action) {
    
}

const store = createStore((state, action) => {
    return state;
});
