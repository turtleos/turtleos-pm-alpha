import {Permission} from '../../lib/virtfs/virtfs.js';

function DEFAULT_PERMISSION(type) {
    switch(type) {
    case 'root-only':
        return new Permission({
        root: {
            canRead: true,
            canWrite: true,
            canMake: true
        },
        user: {},
        });
        break;
    case 'all':
        return new Permission({
        user: {
            canRead: true,
            canWrite: true,
            canMake: true,
            canDelete: true
        },
            root: {
            canRead: true,
            canWrite: true,
            canMake: true,
            canDelete: true
            }
        });
    default:
        return new Permission({
            root:{},
            user:{}
        });
    }
};

export {DEFAULT_PERMISSION}
