import {
    File,
    Folder,
    Permission
} from '../../lib/virtfs/virtfs.js';

import {DEFAULT_PERMISSION} from './permissive.js';

const ROOTFS = {
    etc: new Folder({
        permission: DEFAULT_PERMISSION('root-only'),
        content: {
            hostname: new File({
                bType: 'txt',
                content: 'My TurtleOS Device',
                permission: DEFAULT_PERMISSION('root-only')
            }),
            hosts: new File({
                bType: 'json',
                content: `{hosts:[{url:'*.turtle', redirect:'.sub.turtleos.ccw.icu'}]}`,
                permission: DEFAULT_PERMISSION('root-only')
            }),
        }
    })
};
export {ROOTFS};
